package ist.challenge.zainimuhaimin.features.login.service;

import ist.challenge.zainimuhaimin.features.login.model.LoginResDto;
import ist.challenge.zainimuhaimin.model.UserModel;
import ist.challenge.zainimuhaimin.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    @Autowired
    UserRepo userRepo;

    public ResponseEntity<LoginResDto<UserModel>> login(UserModel body){
        String username = body.getUsername();
        String password = body.getPassword();

        LoginResDto loginResDto = new LoginResDto();

        if (username == null || password == null){
            loginResDto.setMessage("username atau password kosong");
            loginResDto.setStatus(false);
            loginResDto.setPayload(body);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(loginResDto);
        }


         if (userRepo.findByUsername(username) == null || userRepo.findByPassword(password) == null){
            loginResDto.setMessage("username atau password tidak cocok");
            loginResDto.setStatus(false);
            loginResDto.setPayload(body);

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(loginResDto);
        }

        var getUserId = userRepo.findByPassword(password);
        var getUser = userRepo.findById(getUserId.getId());
        loginResDto.setMessage("sukses login");
        loginResDto.setStatus(true);
        loginResDto.setPayload(getUser);

        return ResponseEntity.ok(loginResDto);
    }
}
