package ist.challenge.zainimuhaimin.features.login.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResDto<T> implements Serializable {
    private Boolean status;
    private String message;
    private T payload;
}
