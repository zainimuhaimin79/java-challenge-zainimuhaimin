package ist.challenge.zainimuhaimin.features.login.rest;

import ist.challenge.zainimuhaimin.features.login.model.LoginResDto;
import ist.challenge.zainimuhaimin.features.login.service.LoginService;
import ist.challenge.zainimuhaimin.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginRest {

    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    ResponseEntity<LoginResDto<UserModel>> login(@RequestBody UserModel body){
        return loginService.login(body);
    }
}
