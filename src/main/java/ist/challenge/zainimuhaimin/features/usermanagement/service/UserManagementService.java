package ist.challenge.zainimuhaimin.features.usermanagement.service;

import ist.challenge.zainimuhaimin.features.usermanagement.model.UserManagementResDto;
import ist.challenge.zainimuhaimin.model.UserModel;
import ist.challenge.zainimuhaimin.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserManagementService {
    @Autowired
    UserRepo userRepo;

    public List<UserModel> findAll(){
        return userRepo.findAll();
    }

    public ResponseEntity<UserManagementResDto<UserModel>> update(UserModel body){
        String username = body.getUsername();
        String password = body.getPassword();

        UserManagementResDto userManagementResDto = new UserManagementResDto();

        if (userRepo.findByUsername(username) != null){
            userManagementResDto.setStatus(false);
            userManagementResDto.setMessage("Username sudah terpakai");
            userManagementResDto.setPayload(body);

            return ResponseEntity.status(HttpStatus.CONFLICT).body(userManagementResDto);
        }


        if (userRepo.findByPassword(password) != null){
            userManagementResDto.setStatus(false);
            userManagementResDto.setMessage("Password tidak boleh sama dengan password sebelumnya");
            userManagementResDto.setPayload(body);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(userManagementResDto);
        }

        UserModel userModel = new UserModel();
        userModel.setUsername(body.getUsername());
        userModel.setPassword(body.getPassword());

        userManagementResDto.setStatus(true);
        userManagementResDto.setMessage("Berhasil");
        userManagementResDto.setPayload(userRepo.save(body));

        return ResponseEntity.status(HttpStatus.CREATED).body(userManagementResDto);
    }
}
