package ist.challenge.zainimuhaimin.features.usermanagement.rest;

import ist.challenge.zainimuhaimin.features.usermanagement.model.UserManagementResDto;
import ist.challenge.zainimuhaimin.features.usermanagement.service.UserManagementService;
import ist.challenge.zainimuhaimin.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserManagementRest {

    @Autowired
    UserManagementService userManagementService;

    @GetMapping("/getUsers")
    List<UserModel> findAll(){
        return userManagementService.findAll();
    }

    @PutMapping("/updateUser")
    ResponseEntity<UserManagementResDto<UserModel>> update(@RequestBody UserModel body){
        return userManagementService.update(body);
    }
}
