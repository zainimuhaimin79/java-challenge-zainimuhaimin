package ist.challenge.zainimuhaimin.features.registration.rest;

import ist.challenge.zainimuhaimin.model.UserModel;
import ist.challenge.zainimuhaimin.features.registration.model.RegistrationResDto;
import ist.challenge.zainimuhaimin.repository.UserRepo;
import ist.challenge.zainimuhaimin.features.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationRest {

    @Autowired
    private RegistrationService registrationService;

    @PostMapping("/registration")
    ResponseEntity<RegistrationResDto<UserModel>> registration(@RequestBody UserModel body){
        return registrationService.save(body);
    }

}
