package ist.challenge.zainimuhaimin.features.registration.service;

import ist.challenge.zainimuhaimin.model.UserModel;
import ist.challenge.zainimuhaimin.features.registration.model.RegistrationResDto;
import ist.challenge.zainimuhaimin.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

   @Autowired
   private UserRepo userRepo;


   public ResponseEntity<RegistrationResDto<UserModel>> save(UserModel body){
      String username = body.getUsername();
      String password = body.getPassword();
      RegistrationResDto registrationResDto = new RegistrationResDto();
      if (userRepo.findByUsername(username) != null){
         registrationResDto.setStatus(false);
         registrationResDto.setMessage("Username sudah dipakai");
         registrationResDto.setPayload(body);

         return ResponseEntity.status(HttpStatus.CONFLICT).body(registrationResDto);
      }

      if (username == null || password == null){
         registrationResDto.setMessage("username atau password kosong");
         registrationResDto.setStatus(false);
         registrationResDto.setPayload(body);

         return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(registrationResDto);
      }

      UserModel userModel = new UserModel();
      userModel.setUsername(body.getUsername());
      userModel.setPassword(body.getPassword());

      registrationResDto.setStatus(true);
      registrationResDto.setMessage("Berhasil");
      registrationResDto.setPayload(userRepo.save(body));

      return ResponseEntity.status(HttpStatus.CREATED).body(registrationResDto);
   }
}
