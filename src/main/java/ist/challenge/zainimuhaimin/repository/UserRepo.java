package ist.challenge.zainimuhaimin.repository;

import ist.challenge.zainimuhaimin.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<UserModel, Long> {

     UserModel findByUsername(String username);

     @Query(value = "select id,username,password from users where password= ?1 ", nativeQuery = true)
     UserModel findByPassword(String password);

}
