package ist.challenge.zainimuhaimin.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "users")
public class UserModel implements Serializable {
    private static final long serialVersionUID = 3648346606204855343L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "username", unique = true, length = 25)
    private String username;

    @NotNull
    @Column(name = "password", unique = false, length = 25)
    private String Password;
}
