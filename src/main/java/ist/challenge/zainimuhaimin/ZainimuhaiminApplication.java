package ist.challenge.zainimuhaimin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZainimuhaiminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZainimuhaiminApplication.class, args);
	}

}
